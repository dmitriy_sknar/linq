﻿using System;

namespace Linq.Model {
    //schema
    //"id": 0,
    //"name": "string",
    //"description": "string",
    //"createdAt": "2020-07-06T12:50:14.735Z",
    //"finishedAt": "2020-07-06T12:50:14.735Z",
    //"state": 0,
    //"projectId": 0,
    //"performerId": 0
    public class BsaTask {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        
        public int State { get; set; }
        public TaskState TaskState { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
        
        public int PerformerId { get; set; }
        public User Performer { get; set; }
    }
}