﻿namespace Linq.Model {
    public class TaskState {
        //schema 
        // "id": 0,
        // "value": "string"

        public int Id { get; set; }
        public string Value { get; set; }
    }
}