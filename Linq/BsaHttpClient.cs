﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Linq {
    public static class BsaHttpClient {
        private const string PROJECTS_URI = "https://bsa20.azurewebsites.net/api/projects";
        private const string TASKS_URI = "https://bsa20.azurewebsites.net/api/tasks";
        private const string TASKSTATES_URI = "https://bsa20.azurewebsites.net/api/taskstates";
        private const string TEAMS_URI = "https://bsa20.azurewebsites.net/api/teams";
        private const string USERS_URI = "https://bsa20.azurewebsites.net/api/users";

        public static async Task<string> GetProjectsAsync() {
            return await GetHttpData(PROJECTS_URI);
        }

        public static async Task<string> GetTasksAsync() {
            return await GetHttpData(TASKS_URI);
        }

        public static async Task<string> GetTaskStatesAsync() {
            return await GetHttpData(TASKSTATES_URI);
        }

        public static async Task<string> GetTeamsAsync() {
            return await GetHttpData(TEAMS_URI);
        }

        public static async Task<string> GetUsersAsync() {
            return await GetHttpData(USERS_URI);
        }

        private static async Task<string> GetHttpData(string uri) {
            using var client = new HttpClient();
            HttpResponseMessage response = (await client.GetAsync(uri)).EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
    }
}