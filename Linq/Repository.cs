﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Linq.Model;
using Newtonsoft.Json;

namespace Linq {
    public class Repository {
        public List<Project> ProjectDB;

        public Repository() {
            Console.WriteLine("Loading data from API...");
            Console.WriteLine($"Project count loaded: {Projects.Count}");
            Console.WriteLine($"Tasks count loaded: {Tasks.Count}");
            Console.WriteLine($"Task states count loaded: {TaskStates.Count}");
            Console.WriteLine($"Teams count loaded: {Teams.Count}");
            Console.WriteLine($"Users count loaded: {Users.Count} \n");
            InitDB();
        }

        public void InitDB() {
            ProjectDB = Projects.Select(p => new Project {
                    Id = p.Id,
                    AuthorId = p.AuthorId,
                    TeamId = p.TeamId,
                    CreatedAt = p.CreatedAt,
                    DeadLine = p.DeadLine,
                    Description = p.Description,
                    Name = p.Name,
                })
                .GroupJoin(Tasks,
                    project => project.Id,
                    bsaTask => bsaTask.ProjectId,
                    (project, bsaTaskList) => {
                        bsaTaskList = bsaTaskList.Join(Users,
                            task => task.PerformerId,
                            user => user.Id,
                            (task, user) => {
                                task.Performer = user;
                                return task;
                            });
                        project.Tasks = bsaTaskList.ToList();
                        return project;
                    })
                .Join(Users,
                    project => project.AuthorId,
                    user => user.Id,
                    (project, user) => {
                        project.Author = user;
                        return project;
                    })
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (project, team) => {
                        team.TeamMates = Users
                            .Where(u => u.TeamId == team.Id)
                            .Select(u => u).ToList();
                        project.Team = team;
                        return project;
                    }).ToList();
        }

        private List<Project> _projects;
        public List<Project> Projects {
            get
            {
                if (_projects == null) {
                    _projects = GetProjects().Result;
                }

                return _projects;
            }
        }

        private List<BsaTask> _tasks;
        public List<BsaTask> Tasks {
            get
            {
                if (_tasks == null) {
                    _tasks = GetTasks().Result;
                }

                return _tasks;
            }
        }

        private List<TaskState> _taskStates;
        public List<TaskState> TaskStates {
            get
            {
                if (_taskStates == null) {
                    _taskStates = GetTaskStates().Result;
                }

                return _taskStates;
            }
        }

        private List<Team> _teams;

        public List<Team> Teams {
            get
            {
                if (_teams == null) {
                    _teams = GetTeams().Result;
                }

                return _teams;
            }
        }

        private List<User> _users;

        public List<User> Users {
            get
            {
                if (_users == null) {
                    _users = GetUsers().Result;
                }

                return _users;
            }
        }

        public async Task<List<TaskState>> GetTaskStates() {
            var json = await BsaHttpClient.GetTaskStatesAsync();
            List<TaskState> tastStates = JsonConvert.DeserializeObject<List<TaskState>>(json);
            return tastStates;
        }

        public async Task<List<Team>> GetTeams() {
            var json = await BsaHttpClient.GetTeamsAsync();
            List<Team> teams = JsonConvert.DeserializeObject<List<Team>>(json);
            return teams;
        }

        public async Task<List<User>> GetUsers() {
            var json = await BsaHttpClient.GetUsersAsync();
            List<User> users = JsonConvert.DeserializeObject<List<User>>(json);
            return users;
        }

        public async Task<List<Project>> GetProjects() {
            var json = await BsaHttpClient.GetProjectsAsync();
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(json);
            return projects;
        }

        public async Task<List<BsaTask>> GetTasks() {
            var json = await BsaHttpClient.GetTasksAsync();
            List<BsaTask> tasks = JsonConvert.DeserializeObject<List<BsaTask>>(json);
            return tasks;
        }
    }
}