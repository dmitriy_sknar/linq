﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linq.Model;

namespace Linq {
    public class ConsoleQueryInteractor {
        private Repository _repo;
        private Dictionary<UiActions, string> ActionsDictionary;

        private void InitRepo() {
            _repo = new Repository();
        }

        public void Start() {
            InitRepo();
            InitActions();
            GreatUser();
            Execute();
        }

        private void Execute() {
            ShowActions();
            UiActions selectedAction = PromptUser();
            ExecuteAction(selectedAction);
            AskToContinue();
        }

        private void AskToContinue() {
            Console.WriteLine("\nDo you want to continue: yes (Y) \\ no (N)?");
            var key = Console.ReadKey().KeyChar;
            if (key == 'y' || key == 'Y') {
                Console.WriteLine();
                Execute();
            } else if (key == 'n' || key == 'N') {
                return;
            } else {
                Console.WriteLine("Please select right choice!");
                AskToContinue();
            }
        }

        private void ShowActions() {
            Console.WriteLine("\nQuery master actions:");
            for (int i = 0; i < ActionsDictionary.Count; i++) {
                Console.WriteLine($"{i + 1} - {ActionsDictionary.ElementAt(i).Value}");
            }
        }

        private void GreatUser() {
            Console.WriteLine("Query master greats you!");
        }

        private UiActions PromptUser() {
            Console.WriteLine("\nExecute an action:");
            var input = Console.ReadLine();
            if (Int32.TryParse(input, out int actionId) && actionId > 0 && actionId <= ActionsDictionary.Count)
                return (UiActions)actionId;
            else {
                Console.WriteLine("Please enter valid action.");
                PromptUser();
            }

            return UiActions.NoAction;
        }

        private void InitActions() {
            ActionsDictionary = new Dictionary<UiActions, string> {
                { UiActions.StartQueries, "Run all queries. As no place to use result - debug code to see results!" }
            };
        }

        private void ExecuteAction(UiActions selectedAction) {
            switch (selectedAction) {
                case UiActions.StartQueries:
                    int userId = 1;
                    var dict1 = TasksPerUserProject(userId);

                    userId = 2;
                    var list2 = TasksPerUserWithNameLengthUpTo45Symbols(userId);

                    var list3 = TasksPerUserFinishedIn2020(userId);

                    var list4 = GetCommandsWithUsersOlder10Years();

                    var list5 = GetUsersSortedByFirstNameAndTaskNameLength();

                    int projectId = 6;
                    var list6 = GetUserAndProjectsDetails(projectId);

                    var list7 = GetProjectWithDetails();

                    Console.WriteLine("<< 1 dictionary and 6 lists were successfully created. Debug to see them! >>");
                    break;

                default:
                    Console.WriteLine("Such action is not supported");
                    break;
            }
        }

        //1. Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        private Dictionary<Project, int> TasksPerUserProject(int userId) {
            return _repo.ProjectDB.Where(p => p.AuthorId == userId)
                .Select(p => new KeyValuePair<Project, int>(p, p.Tasks.Count))
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        // 2. Получить список тасков, назначенных на конкретного пользователя(по id), где name таска< 45 символов (коллекция из тасков).
        private List<BsaTask> TasksPerUserWithNameLengthUpTo45Symbols(int userId) {
            return _repo.ProjectDB
                .Where(p =>
                    p.Tasks.Any(
                        t => t.Name.Length < 45
                             && t.PerformerId == userId))
                .SelectMany(p => p.Tasks)
                .ToList();
        }

        // 3. Получить список(id, name) из коллекции тасков, которые выполнены(finished) в текущем(2020) году для конкретного пользователя(по id).
        private List<(int, string)> TasksPerUserFinishedIn2020(int userId) {
            return _repo.ProjectDB
                .SelectMany(p => p.Tasks)
                .Where(t => DateTime.Compare(t.FinishedAt, DateTime.Now) <= 0
                                 && t.PerformerId == userId)
                .Select(task => (task.Id, task.Name))
                .ToList(); 
        }
        // 4. Получить список(id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет,
        // отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
        //     P.S - в этом запросе допускается проверить только год рождения пользователя, без привязки к месяцу/дню/времени рождения.

        private List<(int, string, List<User>)> GetCommandsWithUsersOlder10Years() {
            return _repo.ProjectDB
                .Where(proj => proj.Team.TeamMates
                    .Any(mate => DateTime.Compare(mate.Birthday, DateTime.Today.AddYears(-10)) <= 0))
                .Select(p => p.Team)
                .Distinct()
                .Select(t => (
                    t.Id, 
                    t.Name,
                    t.TeamMates.OrderByDescending(u => u.RegisteredAt) //по дате регистрации пользователя по убыванию - did you mean this? 
                        .ToList()
                )).ToList();
        }

        // 5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        private List<User> GetUsersSortedByFirstNameAndTaskNameLength() {
            return _repo.ProjectDB
                .SelectMany(p => p.Tasks)
                .Select(task => task.Performer)
                .Distinct()
                .OrderBy(user => user.FirstName)
                .GroupJoin(_repo.ProjectDB.SelectMany(p => p.Tasks),
                    user => user.Id,
                    bsaTask => bsaTask.PerformerId,
                    (user, bsaTaskList) => {
                        bsaTaskList = bsaTaskList.OrderByDescending(task => task.Name.Length);
                        user.Tasks = bsaTaskList.ToList();
                        return user;
                    }).ToList();
        }

        // 6.Получить следующую структуру (передать Id пользователя в параметры):
        // User
        // Последний проект пользователя(по дате создания)
        // Общее кол-во тасков под последним проектом
        // Общее кол-во незавершенных или отмененных тасков для пользователя
        // Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)
        // P.S. - в данном случае, статус таска не имеет значения, фильтруем только по дате.
        private (User, Project, int, int, BsaTask) GetUserAndProjectsDetails(int userId) {
            return _repo.ProjectDB
                    .Where(proj => proj.AuthorId == userId)
                    .OrderByDescending(proj => proj.CreatedAt)
                    .Take(1)
                    .Select(proj => (
                        proj.Author,
                        proj,
                        proj.Tasks.Count,
                        proj.Tasks
                            .Where(t => t.FinishedAt == null)
                            .Select(t => t)
                            .Count(),
                        proj.Tasks
                            .OrderByDescending(t => (t.FinishedAt.Subtract(t.CreatedAt)))
                            .FirstOrDefault()
                    )).FirstOrDefault();
        }

        //7. Получить следующую структуру:
        // Проект
        // Самый длинный таск проекта(по описанию)
        // Самый короткий таск проекта(по имени)
        // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков< 3
        private List<(Project, BsaTask, BsaTask, int)> GetProjectWithDetails() {
            return _repo.ProjectDB
                .Select(pr => {
                    int teamMatesCount = 0;
                    if (pr.Name.Length > 20 || pr.Tasks.Count < 3) {
                        teamMatesCount = pr.Team.TeamMates.Count;
                    }

                    return (
                        pr,
                        pr.Tasks
                            .OrderByDescending(t => t.Description.Length)
                            .FirstOrDefault(),
                        pr.Tasks
                            .OrderBy(t => t.Name.Length)
                            .FirstOrDefault(),
                        teamMatesCount
                    );
                })
                .ToList();
        }
    }
}